
/* JavaScript Foundations: Objects
   _________________________________________________________________________ */

// Call and Apply
// We can control which object a method is associated with when we call the
// method by using the special call and apply functions.

var me = {
  name:  'David',
  greet: function() { console.log('Hello, I am', this.name) },
  moody: function(name, mood) {
    name = name || 'You';  // Optional assignation
    mood = mood || 'good';

    console.log('Hello, ' + name + ' I am ' + this.name + ' and I am in a ' +
      mood + ' mood!');
  }
};

var bf = { name: 'Juanjo', greet: me.greet };

me.greet();
bf['greet']();

// As seen before, when the function is outside and object, it may lose
// the context.
var greet = me.greet;
greet();

// But, the call function allows to bind and call the function to an specific
// object.
greet.call(me);
greet.call(bf);
greet.call({ name: 'Omar' });
// The function can be called from any reference to it.
me.greet.call({ name: 'Omar Francisco' });

// Apply works in a similar way, but receives arguments inside an array.
// This allows to pass parameters dynamically.
me.moody.call(bf, 'David', 'bad');
me.moody.apply(bf, ['David', 'ok']);
