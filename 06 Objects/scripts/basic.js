
/* JavaScript Foundations: Objects
   _________________________________________________________________________ */

// Basic Objects
// Creating a basic object is simple. It stores any number of values associated
// with string "keys." JavaScript has a special object literal syntax that makes
// constructing new objects with keys and values easy.

var me = {
  // Object attribute keys can be strings or valid variable names.
  'name': 'David',
  skills: ['JavasScript', 'Ruby', 'Learning'],
  'favorite color': 'Red'
};

console.log(me.name);
console.log(me.skills);

// To retrieve attributes with compound names, the square brackets are used.
console.log(me['favorite color']);

me.name = 'David O';
console.log(me);
