
/* JavaScript Foundations: Objects
   _________________________________________________________________________ */

// Methods
// We can store functions as values on objects. When we do this, we refer to the
// function as a method associated with the object.

var me = {
  name:  'David',
  // this is a pseudo variable that references to the object executing the code.
  greet: function() { console.log('Hello, I am', this.name) }
};

var bf = {
  name:  'Juanjo',
  greet: me.greet
}

me.greet();
bf.greet();

// If the function is referenced outside and object, this may lose context.
var greet = me.greet;
greet();

// What is the global object in a browser?
(function(){ console.log(this); })(); // It is the Window object!
