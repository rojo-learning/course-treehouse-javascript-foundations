
/* JavaScript Foundations: Objects
   _________________________________________________________________________ */

// Prototypes I
// We can reuse common objects in our applications by using prototypes. We can
// create new objects based on other objects and extend them to have new
// properties while retaining the original properties.

var personPrototype = {
  name:    'Anonymous',
  species: 'Homo Sapiens',

  greet: function(name, mood) {
    name = name || 'You';
    mood = mood || 'good';

    console.log('Hello, ' + name + '. I am ' + this.name + ' and I am in a ' +
      mood + ' mood!');
  }
};

function Person(name) { this.name = name; };

Person.prototype = personPrototype;

dave = new Person('David');
dave.greet();
