
/* JavaScript Foundations: Objects
   _________________________________________________________________________ */

// This knows which is its prototype.
var personPrototype = { name: 'Anonymous', species: 'homo sapiens'};

function Person(name) { this.name = name; };
Person.prototype = personPrototype;

dave = new Person('David');
console.log(dave.name);
juan = new Person('Juanjo');
console.log(juan.name);

// Modifying the prototype changes the values on the objects that inherit from
// it, so it must be done with caution.
console.log(dave.species);
console.log(juan.species);

personPrototype.species = 'human';
Person.prototype.gender = 'male'; // Other way of reference

console.log(dave.species);
console.log(juan.gender);
