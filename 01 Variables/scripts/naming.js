
/* JavaScript Foundations: Variables
   _________________________________________________________________________ */

/**
  Variable names can start with A-Z, a-z, _ or $. Then it may continue with A-Z,
  a-z 0-9, _ or $.
  It is better for them to be given meaningful concise names.
*/

// Valid variables
var car          = 'Toyota';
var Color        = 'blue';
var _myVariable  = 'something';
var $specialName = 1;
var a58389       = 'What is this?';

// Invalid variable names
// var 3colors     = 'red green blue';
// var winning%    = 30;
// var person-name = 'David';
// var @you        = 'are awesome';

// JavaScript reserved words can't be used as variable names
// var continue = true;
