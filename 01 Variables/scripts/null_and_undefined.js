/* JavaScript Foundations: Variables
   _________________________________________________________________________ */

/**
  JavaScript uses two special values:
   - undefined: declared variables with no value set
   - null: keyword which means nothing or empty
*/

var myVar;
var x = null;

// Testing if a variable is undefined
// Right way
console.log(typeof myVar === 'undefined');
// Wrong! undefined is a variable whose value can change
// undefined = true;
console.log(myVar === undefined);

// undefined and null evaluate to false
if(myVar) {
  console.log('If myVar');
} else {
  console.log('Else myVar');
};

if(x) {
  console.log('If x');
} else {
  console.log('Else x');
};

// undefined and null are different, but they are similar.
// we can check if a variable has is either undefined or null
console.log(undefined == null);
// or check the for one of those value specifically
console.log(undefined === null);
