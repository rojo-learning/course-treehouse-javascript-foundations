/* JavaScript Foundations: Variables
   _________________________________________________________________________ */

// Scope
// Some variables can't be used anywhere, 'cause they have a scope

// Global scope: They are know everywhere
var world = 'World!';

function sayHello() {
  // Local scope: They are know only inside the function body
  var hello = 'Hello ';

  function inner() {
    var extra = ' There is more!'
    // `hello` known inside and outside `inner`
    // `inner` is only know inside `inner`
    console.log(hello + world + extra);
  }
};

sayHello();

// `world` is in the global scope
console.log("world outside sayHello():", world);
// but `hello` is not...
// console.log("hello outside sayHello():", hello);
