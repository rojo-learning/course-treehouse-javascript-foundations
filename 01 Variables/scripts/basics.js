
/* JavaScript Foundations: Variables
   _________________________________________________________________________ */

// A variable can hold the result of any expression
var color = 'black';

// Like an element from an HTML document
var div = document.getElementById('my_div');
    // And the variable can be referenced any number of times
    div.style.background = color;
    div.style.color      = '#fff';
