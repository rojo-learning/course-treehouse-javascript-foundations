
/* JavaScript Foundations: Variables
   _________________________________________________________________________ */

// Hoisting
// JavaScript has no block scope but function scope
function doSomething(doit) {
  var color = 'blue';

  if (doit) {
    // The color var is rewritten, ooops!
    var color = 'red';
    console.log('Color in if(){}', color);
  };

  console.log('Color after if(){}', color);
};

doSomething(false);
doSomething(true);

function doSomethingElse(doit) {
  if (doit) {
    var color = 'yellow';
    console.log('Color in if(){}', color);
  };

  // color is not declared in this path but it is known at the function
  // so it just remains unassigned. This is hoisting.
  console.log('Color after if(){}', color);
};

doSomethingElse(false);
doSomethingElse(true);

// Several variables can be declared nicely with one var statement and comas
var name   = 'David',
    age,
    height = 188;
