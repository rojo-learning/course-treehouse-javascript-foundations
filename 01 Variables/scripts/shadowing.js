/* JavaScript Foundations: Variables
   _________________________________________________________________________ */

// Shadowing
// Variables with the same name in different scopes can exist. But the variable
// of the local scope shadows the variables from outter scopes.

var myColor = 'blue';
console.log('myColor before myFunc()', myColor);

function myFunc() {
  var myColor = 'yellow';
  console.log('myColor inside myFunc()', myColor);
};

myFunc();

console.log('myColor after myFunc()', myColor);

// Assigning values to undefined variables without the preceding the `var` word,
// defines them in a global scope but may have unexpected behaviours.

function notOk() {
  // Don't forget to use `var`!
  may_bug_you = 'I may come to haunt you and cause headaches';
}
