# Treehouse - JavaScript Foundations #

This repository contains notes and practice examples from **JavaScript Foundations**, imparted by Jim Hoskins at [Threehouse][JF].

> Learn the fundamentals to the JavaScript Language. JavaScript is the key to programming in the browser, mastering jQuery, and building apps with Node.js. You will learn how to program effectively in JavaScript, the different types, functions, and techniques for writing solid JavaScript. You will benefit from taking Introduction to Programming if you don't have much experience with programming.

## Contents ##

- **Variables**: The JavaScript Variables badge will teach you everything you need to know about variables, how they are created and used, and how JavaScript handles some special cases of variable creation.
- **Strings**: Strings are how we represent text in JavaScript. The JavaScript language provides a lot of great tools for creating and manipulating strings, and this stage will teach you what you need to know to work with strings.
- **Numbers**: Numbers are an important part of any program. JavaScript provides several useful techniques for dealing with numbers.
- **Arrays**: Arrays allow us to store large numbers of values within a single variable. They act like lists which allow us to group our data together in an ordered fashion.
- **Functions**: Functions are a foundational concept in JavaScript. Functions allow us to store code for reuse in our programs. This allows us to organize our code and make it much easier to maintain and write.
- **Objects**: Objects are the foundation of JavaScript. They are very easy to learn, but can be a bit difficult to master. In this course we will look at simple JavaScript objects as well as how to use Prototypes to do object oriented programming.

---
This repository contains code examples from Treehouse. These are included under fair use for showcasing purposes only. Those examples may have been modified to fit my particular coding style.

[JF]: http://teamtreehouse.com/library/javascript-foundations
