
/* JavaScript Foundations: Functions
   _________________________________________________________________________ */

// Arguments
// Functions in JavaScript allow us to store code for reuse later in our
// programs. We can change the behavior each time we use the function by
// passing in data called arguments.

// Arguments goes inside the parenthesis. The function body goes inside
// brackets.
function sayHello() {
  console.log('Hello, World!');
};

// The function must be called to be executed.
sayHello();

// The values received as parameters can be used inside of the function
function greetSomeone(name, greeting) {
  if (typeof greeting === 'undefined') { greeting = 'Hello,'; };
  console.log(greeting + ' ' + name + '!');
};

greetSomeone('David', 'Greets');

// A function can accept fewer or more attributes than the ones declared on its
// definition. Extra params are ignored and the ones missing are undefined.
greetSomeone('David');
