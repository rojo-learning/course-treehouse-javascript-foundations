
/* JavaScript Foundations: Functions
   _________________________________________________________________________ */

// Anonymous Functions
// We can also declare functions without names, called anonymous functions,
// which are useful when we want to use a function once, or simply store it in
// a variable.

var myFunction = function() {
  console.log('my_function was called');
};

// The function doesn't have a name but is referenced by the variable.
myFunction();

// Functions can be passed as parameters for other functions
var callTwice = function(targetFunction) {
  targetFunction();
  targetFunction();
};

callTwice(myFunction);

// Anonymous functions can execute themselves intermediately, creating scopes
// temporally. These scopes will disappear when the function finishes.
(function (){
  var a, b, c;
  // ...
  console.log('from anon function: was');
})(1, 'hello');

// A problem may be that if an error occurs, the stack will lead to a function
// with no name, making it harder to debug.
callTwice(function(){ undeclaredVariable; });
