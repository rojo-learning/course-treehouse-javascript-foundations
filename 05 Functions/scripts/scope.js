
/* JavaScript Foundations: Functions
   _________________________________________________________________________ */

// Scope
// We can declare variables inside of our functions, and they have a unique
// property: they exist only within that function. We call this scope, and it
// has some very important implications in our programs.

var color  = 'black';
var number = 1;
function showColor() {
  // Variable shadowing: has the same name of an outer scope variable.
  var color = 'green';

  // Variables declared inside functions belong to the function scope.
  console.log('showColor color:', color);
  // Variables from outer scopes can be accessed from inner scopes.
  number = 2;
  console.log('showColor number:', number);

  // Referencing a non-declared variable without the var keyword to declare it
  // will set that variable in the global scope. It is seem as a bad practice.
  shape = 'square';
};

showColor();
console.log('Global color:', color);
console.log('Global number:', number);
console.log('Global shape:', shape);
