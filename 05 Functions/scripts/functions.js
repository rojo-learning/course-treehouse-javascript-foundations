
/* JavaScript Foundations: Functions
   _________________________________________________________________________ */

// Examples

var button = document.getElementById('action');
var input  = document.getElementById('text_field');

// Several event listeners can be added to an element.
button.addEventListener('click', function() {
  console.log('clicked');
});

button.addEventListener('click', function() {
  console.log('other click');
  input.setAttribute('value', 'Hello World');
});
