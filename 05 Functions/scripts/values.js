
/* JavaScript Foundations: Functions
   _________________________________________________________________________ */

// Return Values
// Our functions can calculate values that we call the return value. The return
// statement not only allows us to pass the value back to the caller of our
// function, but also stops the execution of the function itself.

function sayHello (name, greeting) {
  if (typeof name === 'undefined') {
    // Is importan to rememeber that any code after the return instruction
    // won't be executed at all.
    return 0;
  };

  if (typeof greeting === 'undefined') { greeting = 'Hello'; };
  console.log(greeting + ' World! ' + name);

  return name.length;
};

console.log(sayHello('David', 'Greetings'));
console.log(sayHello('Juan José'));
console.log(sayHello());
