
/* JavaScript Foundations: Numbers
   _________________________________________________________________________ */

// Comparison Operators
console.log(1 < 2);  // less than
console.log(1 > 2);  // greater than
console.log(1 <= 1); // less or equal than
console.log(2 >= 2); // greater or equal than

// The equal operator must be used wisely, as when comparing a number to a
// string, can have unexpected results
console.log(1 ==  1.0);
console.log(1 === 1.0);

console.log(1 ==  '1');
console.log(1 === '1'); // checks value and type

// Also, we can check for inequality
console.log(1 != '1');
console.log(1 !==  3);
