
/* JavaScript Foundations: Numbers
   _________________________________________________________________________ */

// The Math Object allows to perform several mathematical operations and have
// some mathematical constants at hand.

// Random numbers
var a = Math.random(); // a decimal number bwtween 0 and 1

// Rouding numbers
var b = Math.round(a * 10); // Ups from .5, downs from bellow .5
var c = Math.ceil(a * 10);  // Ups from .1 to the nearest integer
var d = Math.floor(a * 10); // Downs from .9 to the nearest integer

// Exponentiation and square root
var e = Math.pow(2, 5);
var f = Math.sqrt(4);

// Absolute value
var g = Math.abs(-1);

// PI constan value
var pi = Math.PI;

// It also includes other logarithmic and trigonometric functions that can be
// checked in the JavaScript console.
