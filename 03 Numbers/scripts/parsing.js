
/* JavaScript Foundations: Numbers
   _________________________________________________________________________ */

// parseInt obtains whole numbers from strings.
var a = parseInt('197', 10); // string, base

// It is better to always indicate the base of the number to avoid unexpected
// behaviors.
var b1 = parseInt('012');     // => 10 from octal
var b2 = parseInt('019');     // => 1
var b3 = parseInt('019', 10); // => 19

// Of course, other bases can be used.
var c = parseInt('010111', 2); // => 23 from binary

// The parse will start from the beginning of the string so...
var d1 = parseInt('23 people');           // => 23
var d2 = parseInt('there are 23 people'); // => NaN (Not a number)

// Operations with NaN will always result in NaN, so if an operation gets this
// result, is better to look where this result could be obtained first.
var e = NaN + 10;

// NaN can be tested with a special function called isNaN(), because NaN has a
// special property making it not equal to itself.
var f = isNaN(e);

// parseFloat will attempt to get a decimal number out of a string, and will
// behave like parseInt for mixed strings, except that the base will be always
// 10.
var g = parseFloat('121.456');
var h = parseFloat('121.456 bitcoins');
