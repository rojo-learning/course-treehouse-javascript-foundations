
/* JavaScript Foundations: Numbers
   _________________________________________________________________________ */

// In JavaScript there is only one type of number. When a whole number is
// assigned, it is represented internally as a decimal number.

var a =  11,
    b = -123;

var c = 1.5,
    d = 123.456179;

// Results of decimal number operations are not exact, due to the way they are
// stored in a computer.

var e = 0.1,
    f = 0.2;

var result = e * f;

// But results of 'integers' are neatly presented. Division always return
// decimal point results.

var g = 1,
    h = 2;

var result2 = g / h;
