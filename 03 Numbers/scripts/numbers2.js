
/* JavaScript Foundations: Numbers
   _________________________________________________________________________ */

// Numbers can also be defined using exponential notation
var a = 1000000,
    b = 1E6,
    c = 1.23E16;

// Numbers declared starting with a leading 0, are considered octal...
var d = 012; // => 10
// ...execpt if when using a number not in the octal base
var e = 019; // => 19

// Numbers can be declared in hexadecimal, leading its declaration with x0
var f = 0x10; // => 16
