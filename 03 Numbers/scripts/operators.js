
/* JavaScript Foundations: Numbers
   _________________________________________________________________________ */

// Arithmetic Operators
var a =  1 + 2,  // addition
    b = 10 - 7,  // subtraction
    c =  3 * 4,  // multiplication
    d = 16 / 8,  // division
    e = 15 % 10; // modulo

// Operators have different order, but parenthesis can be used to alter it
var f = 1 + 2 * 3 / 4;
var g = (1 + 2) * 3 / 4;
