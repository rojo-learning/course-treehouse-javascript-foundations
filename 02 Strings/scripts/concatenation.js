
/* JavaScript Foundations: Strings
   _________________________________________________________________________ */

// Concatenation
var part1 = 'Hello ';
var part2 = 'World!';
var whole = part1 + part2;

console.log(whole);
console.log(whole + '!!!!!');

// When concatenating strings, the parts can be split over several lines.
var multiline = "Thils is line 1\n" + "Thils is line 2\n" +
  "Thils is line 3\n";

console.log(multiline);
