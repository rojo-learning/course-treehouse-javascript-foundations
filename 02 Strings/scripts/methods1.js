
/* JavaScript Foundations: Strings
   _________________________________________________________________________ */

var whole = 'Hello World!';

// String properties
var length = whole.length;
console.log(length);

// String methods
var index1 = whole.indexOf('World'); // Looking for a substring in the string
console.log(index1); // Returns the index where the substring starts

var index2 = whole.indexOf('world'); // This method is case sensitive
console.log(index2); // Returns -1 if no substring is found

if (whole.indexOf('W') !== -1) {
  console.log('W exists in the string.');
} else {
  console.log('W does not exist.');
};

// Get a char on a given index
letter = whole.charAt(6);
console.log(letter);
