
/* JavaScript Foundations: Strings
   _________________________________________________________________________ */

// Double quotes
var name1 = "David";
console.log(name1);

var statement1 = "This is David's string";
console.log(statement1);

// Single quotes
var name2 = 'David';
console.log(name2);

var statement2 = 'He said "This is awesome"';
console.log(statement2);

// Scape sequence inside the string
var statement3 = 'He said "This is David\'s string"';
console.log(statement3);
