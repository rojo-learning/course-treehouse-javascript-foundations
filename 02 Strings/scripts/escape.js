
/* JavaScript Foundations: Strings
   _________________________________________________________________________ */

// Other scape characters

// The backslash indicates a scape secuence, so it must be scaped to be used.
var path = 'C:\\folder\\myfile.txt';
console.log(path);

// JavaScript does nos support strings spanning over multiple lines but the
// newline scape sequence can be used
var multiline = "This is line 1.\nThis is line 2.\nThis is line 3.";
console.log(multiline);
