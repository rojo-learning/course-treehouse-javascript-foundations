/* JavaScript Foundations: Strings
   _________________________________________________________________________ */

// Comparing strings

var first  = 'Hello';
var second = 'hello';

// Returns true if the strings are exactly equal
if (first === second) {
  console.log('They strings are equal');
} else {
  console.log('They strings are different');
};

// When the case is not important, usually both strings case is changed
if (first.toUpperCase() === second.toUpperCase()) {
  console.log('They strings are equal');
} else {
  console.log('They strings are different');
};

// Strings are compared by its order in the ASCII table
function compare(a, b) {
  console.log(a + ' ' + b, a < b);
};

compare('a', 'b'); // 97, 98
compare('a', 'A'); // 97, 65

// it compares each letter until it finds a difference, if any
compare('apples', 'oranges');
compare('hello', 'hello');
