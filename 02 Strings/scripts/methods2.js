/* JavaScript Foundations: Strings
   _________________________________________________________________________ */

// More string methods
var whole = 'Hello World!';

// Extracting part os a string
var world = whole.substr(6, 5); // start, length
console.log(world);

// Changing the case
var lowercase = whole.toLowerCase();
console.log(lowercase);

var uppercase = whole.toUpperCase();
console.log(uppercase);
