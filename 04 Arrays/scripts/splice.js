
/* JavaScript Foundations: Arrays
   _________________________________________________________________________ */

// Splice
// The splice method is the most powerful method for manipulating arrays. It
// can perform just about any transformation.

var my_array = [0, 1, 2, 3, 4, 5, 6];
console.log(my_array.toString());

// There is a method called «delete» that can set a variable to 'undefined'.
delete my_array[3];
console.log(my_array.toString());

// But, to remove an element from an array (moving the position of the
// following elements), splice can be used.
my_array[3] = 3;
console.log(my_array.toString());

var index  = 3; // position to start operating
var number = 1; // number of elements to be removed

my_array.splice(index, number);
console.log(my_array.toString());

// It also allows to add elements on an index position of the array.
var new_element = 3;
index  = 3; // position to start operating
number = 0; // don't remove any elements

my_array.splice(index, number, new_element);
console.log(my_array.toString());

// We can add as much elements as we want.
var new_elements = [2.3, 2.7];
index  = 3;
number = 0;

my_array.splice(index, number, new_elements);
console.log(my_array.toString());
