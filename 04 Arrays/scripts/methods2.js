
/* JavaScript Foundations: Arrays
   _________________________________________________________________________ */

// Methods II
// There are methods that allow us to order and sort arrays.

var my_array = [10, 44, 32, 100, 0, 44, 3, 4];
console.log(my_array.toString());

// sort, by defualt will treat the array's elements as strings
my_array.sort();
console.log(my_array.toString());

// Providing a comparative function the behavior of sort can be changed
// The function must receive two parameters and return a negative values if the
// second item is bigger, a positive value if the first item is bigger, and
// zero if both items are equal.
my_array.sort(function(a, b) { return a - b; });
console.log(my_array.toString());

// An array's order can be randomized using this function, by letting the
// function provide random negative or positive numers.
my_array.sort(function(a, b) { return Math.random() - 0.5; });
console.log(my_array.toString());

// reverse will flip the array so the last item is the first and so on
my_array.reverse();
console.log(my_array.toString());
