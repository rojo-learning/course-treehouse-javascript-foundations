
/* JavaScript Foundations: Arrays
   _________________________________________________________________________ */

// Arrays represent lists of objects. They are useful to reference more than
// one piece of information using one variable.

var x = ['some', 'extra', 'words', 'here'];
console.log(x);
console.log(x.length);

var y = ['a string', 3, true, ['sub', 2, 4], function(a, b) {return a + b}];
console.log(y);
console.log(y.length);

var z = [];
console.log(z);
console.log(z.length);

// There is an alternative way to create an array with the class constructor.
// It allows to set the length of the array when instancing it.
var a = new Array(10);
console.log(a);
console.log(a.length);
