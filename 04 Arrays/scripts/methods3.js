
/* JavaScript Foundations: Arrays
   _________________________________________________________________________ */

// Methods III
// Some more of the methods we can use to manipulate and create new Arrays in
// JavaScript without changing them.

var x = [1, 2, 3];
console.log(x.toString());

var y = [4, 5, 6];
console.log(y.toString());

// Concatenating arrays.
var z = x.concat(y);
console.log(z.toString());

// concat can also append a list of loose values to an array and even mix them
// with proper arrays.
console.log(z.concat(7,8,9, [10, 11, 12]).toString());
console.log(z.toString());

// Slice gets a range of values from an array.
var my_array = [0, 1, 2, 3, 4, 5];
console.log(my_array.toString());

var start = 1;
var limit = 4;
var elements = my_array.slice(start, limit);
console.log(elements.toString());
console.log(my_array.toString());

// join will return a string of an array's elements with the passed string
// between them.
var words = ['these', 'are', 'some', 'words'];
console.log(words.toString());

var string = words.join(' - ');
console.log(string);
