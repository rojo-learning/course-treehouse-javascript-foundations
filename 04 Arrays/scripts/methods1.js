
/* JavaScript Foundations: Arrays
   _________________________________________________________________________ */

// Methods
// There are many methods that allow us to manipulate and inspect the contents
// of arrays.

var my_array = [2, 3, 4];
console.log(my_array.toString());

// push adds an element at the end of the array
my_array.push(5);
console.log(my_array.toString());

// pop removes and returns the element at the end of the array
var value = my_array.pop();
console.log(value);
console.log(my_array.toString());

// unshift adds an element at the beginning of the array
my_array.unshift(1);
console.log(my_array.toString());

// pop removes and returns the element at the beginning of the array
var value2 = my_array.shift();
console.log(value2);
console.log(my_array.toString());
