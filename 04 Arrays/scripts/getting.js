
/* JavaScript Foundations: Arrays
   _________________________________________________________________________ */

// Arrays allow us to retrieve and set values at specific points in our list.
// JavaScript uses indicies to refer to the specific locations within the list.

var my_array = ['Hello', 42, true, function(a) { return a * 2 }];
console.log(my_array);

// Getting
// Indexes in JavaScript are 0 based. The index shows how far from the beginning
// an element is.

var word = my_array[0];
console.log(word);

var number = my_array[1];
console.log(number);

var answer = my_array[2];
console.log(answer);

var doubler = my_array[3];
console.log(doubler(10));

console.log(my_array[100]);

// Setting

my_array[1]    = 144;
var new_number = my_array[1];
console.log(new_number);
console.log(number);

// Extending an array
my_array[4] = 'A new string';
console.log(my_array);

my_array[14] = 'A far away string';
console.log(my_array);
console.log(my_array[5]);

// Adding to the end of an array
my_array[my_array.length] = 'The string at the end';
console.log(my_array);
